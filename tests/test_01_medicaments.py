import requests
import sys
sys.path.insert(0, '../')
from ..libs.medicament import Medicament
from ..libs.traitement import Traitement
from ..libs.contact import Contact
from ..libs.rendez_vous import RendezVous


# first endpoint to test

def test_get_test():
    """
    Make sure a POST at '/test' returns the expected output.
    """
    expected_data = [
        {"retour": "Numéro 0"},
        {"retour": "Numéro 1"},
        {"retour": "Numéro 2"},
        {"retour": "Numéro 3"}
    ]
    response = requests.get("http://localhost:8000/test")
    assert response.json() == expected_data, response.text

def test_get_traitements():
    """
    Make sure that a GET at '/get/traitements/' returns the content that's stored in the database.
    """
    # start with reading from the database
    data = Traitement.query_traitements("all")
    # then GET the data using the API
    response = requests.get("http://localhost:8000/get/traitements/")
    assert response.json() == data, response.text

def test_get_medicaments():
    """
    Make sure that a GET at '/get/traitements/' returns the content that's stored in the database.
    """
    # start with reading from the database
    data = Medicament.query_medicaments("all")
    # then GET the data using the API
    response = requests.get("http://localhost:8000/get/medicaments/")
    assert response.json() == data, response.text


def test_get_contacts():
    """
    Make sure that a GET at '/get/contacts/' returns the content that's stored in the database.
    """
    # start with reading from the database
    data = Contact.query_contacts("all")
    # then GET the data using the API
    response = requests.get("http://localhost:8000/get/contacts/")
    assert response.json() == data, response.text

def test_get_rendez_vous():
    """
    Make sure that a GET at '/get/traitements/' returns the content that's stored in the database.
    """
    # start with reading from the database
    data = RendezVous.query_rendez_vous()
    # then GET the data using the API
    response = requests.get("http://localhost:8000/get/rendez_vous/")
    assert response.json() == data, response.text


