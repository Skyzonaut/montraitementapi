#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import Optional

from fastapi import FastAPI

from .libs.database_management import BDDConnection
from fastapi.middleware.cors import CORSMiddleware

from .libs.traitement import Traitement
from .libs.medicament import Medicament
from .libs.contact import Contact
from .libs.rendez_vous import RendezVous

# Here i am gitlab
app = FastAPI()

origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/test")
async def test():
    retour = []
    for i in range(4):
        retour.append({"retour": f"Numéro {i}"})
    return retour


@app.get("/get/traitements/")
async def getTraitements(mode: Optional[str] = "all"):
    print("Bonjour 1111")
    if mode == "all":
        return Traitement.query_traitements("all")


@app.get("/get/medicaments/")
async def getMedicaments(mode: Optional[str] = "all"):
    if mode == "all":
        return Medicament.query_medicaments("all")


@app.get("/get/contacts/")
async def get_contacts(mode: Optional[str] = "all"):
    if mode == "all":
        return Contact.query_contacts("all")

@app.get("/get/rendez_vous/")
async def query_rendez_vous(mode: Optional[str] = "all"):
    if mode == "all":
        return RendezVous.query_rendez_vous()