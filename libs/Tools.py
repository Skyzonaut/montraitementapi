from ..libs.database_management import BDDConnection
from ..config import Database_config as conf

class Tools:

    @staticmethod
    def get_data(request):
        data = []

        bdd = BDDConnection(conf.db_config["name"])

        # Get data
        results = bdd.execute(request["queryBody"])
        resultsHeader = request["queryHeader"]
        print(resultsHeader)
        for row in results:
            item = {}
            for th in range(len(resultsHeader)):
                # print(item[resultsHeader[th]])
                print(row[th])
                item[resultsHeader[th]] = row[th]
            data.append(item)
        bdd.close()

        return data
