from ..libs.tools import Tools
from ..config import Database_config as conf


class RendezVous:

    @staticmethod
    def query_rendez_vous(mode="all"):
        if mode == "all":
            return RendezVous.query_rendez_vous()

    @staticmethod
    def query_rendez_vous():
        request = RendezVous.query_rendez_vous_request_and_header()
        print(request)
        data = Tools.get_data(request)
        print(data)
        return data

    @staticmethod
    def query_rendez_vous_request_and_header(**header):
        # **header est là pour plus tard pouvoir modifie à la main les noms des champs
        query = {}
        if not header:
            query["queryBody"] = f"select u.name, u.surname, s.name || ' ' || m.status, r.day, " \
                                 f"r.startTime, r.endTime, m.city, m.cp, m.adress from rendez_vous as r " \
                                 f"inner join user u on u.id = r.pro " \
                                 f"inner join medecins m on u.id = m.user " \
                                 f"inner join specialite s on s.idSpecialite = m.specialite;"

            query["queryHeader"] = [
                'name',
                'surname',
                'profession',
                'day',
                'startTime',
                'endTime',
                'city',
                'cpp',
                'address'
            ]

        return query
