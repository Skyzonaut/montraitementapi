from ..libs.tools import Tools
from ..config import Database_config as conf


class Traitement:

    @staticmethod
    def query_traitements(mode="all"):
        if mode == "all":
            return Traitement.get_traitements()

    @staticmethod
    def get_traitements():
        request = Traitement.get_traitement_request_and_header()

        return Tools.get_data(request)

    @staticmethod
    def get_traitement_request_and_header(**header):
        # **header est là pour plus tard pouvoir modifie à la main les noms des champs
        query = {}
        if not header:
            query["queryBody"] = f"select traitements.quantity || ' ' || medicament.name || ' ' || traitements.timePerRec || ' fois par ' || traitements.reccurence, " \
                               f"m.name," \
                               f"strftime('%d-%m-%Y', datetime(traitements.startDate/1000, 'unixepoch')), " \
                               f"strftime('%d-%m-%Y', datetime(traitements.endDate/1000, 'unixepoch')) " \
                               f"from traitements " \
                               f"inner join maladies m on traitements.maladie = m.id " \
                               f"inner join medicaments medicament on medicament.id = traitements.med"

            query["queryHeader"] = [
                conf.vocabulaire['traitements']['treatment'],
                conf.vocabulaire['traitements']['illness'],
                conf.vocabulaire['traitements']['startDate'],
                conf.vocabulaire['traitements']['endDate']
            ]

        return query
