import json
from pprint import pprint

from ..config import Database_config as conf

from ..libs.tools import Tools


class Contact:

    @staticmethod
    def query_contacts(mode="all"):

        if mode == "all":
            return Contact.get_contacts()

    @staticmethod
    def get_contacts():
        request = Contact.get_contact_request_and_header()
        raw_request = Tools.get_data(request)
        contact = Contact.format_data_for_component(raw_request)
        return contact

    @staticmethod
    def format_data_for_component(data):
        for row in data:
            days = conf.vocabulaire['days']['fr']
            list_days = [days[i] for i in range(len(days)-1) if row["days"][i] == "1"]
            row["days"] = list_days
        return data


    @staticmethod
    def get_contact_request_and_header(**header):
        # **header est là pour plus tard pouvoir modifie à la main les noms des champs
        query = {}

        if not header:
            query["queryBody"] = f"select u.name || ' ' || u.surname, " \
                                 f"s.name, " \
                                 f"m.status, " \
                                 f"m.adress || ', ' || m.cp || ' ' || m.city, " \
                                 f"h.start, h.end, h.monday || h.tuesday || h.wednesday || h.thursday || h.friday || h.saturday || h.sunday, "\
                                 f"u.email, u.phone " \
                                 f"from contacts " \
                                 f"inner join user u on contacts.contactUser = u.id " \
                                 f"inner join medecins m on u.id = m.user " \
                                 f"inner join specialite s on s.idSpecialite = m.specialite " \
                                 f"inner join horaires h on h.medecin = u.id;"

            query["queryHeader"] = [
                "contact",
                "specialite",
                "status",
                "adress",
                "start",
                "end",
                "days",
                "email",
                "phone"
            ]

        return query
