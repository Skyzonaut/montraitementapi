from ..config import Database_config as conf
from ..libs.tools import Tools


class Medicament:

    @staticmethod
    def query_medicaments(mode="all"):

        if mode == "all":
            return Medicament.get_medicaments()

    @staticmethod
    def get_medicaments():

        request = Medicament.get_medicament_request_and_header()
        return Tools.get_data(request)

    @staticmethod
    def get_medicament_request_and_header(**header):
        # **header est là pour plus tard pouvoir modifie à la main les noms des champs
        query = {}
        if not header:
            query["queryBody"] = f"select medicaments.medID, " \
                                 f"medicaments.name, " \
                                 f"iT.name " \
                                 f"FROM medicaments " \
                                 f"INNER JOIN maladies iT on iT.id = medicaments.maladie"

            query["queryHeader"] = [
                conf.vocabulaire['medicaments']['medID'],
                conf.vocabulaire['medicaments']['name'],
                conf.vocabulaire['maladies']['name']
            ]

        print(query)
        return query
