import json
import sqlite3

class BDDConnection:
    bdd = None
    cursor = None

    def __init__(self, url=None):
        try:
            if url:
                self.connect(url)

        except sqlite3.Error as e:
            print(e.__getattribute__("message"))


    def execute(self, query):
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def connect(self, url):
        self.bdd = sqlite3.connect(url)
        self.cursor = self.bdd.cursor()


    def close(self):
        self.bdd.close()
