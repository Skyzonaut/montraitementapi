
db_config = {
    "name": "monTraitement.sqlite",
    "relative_path": "./"
}

vocabulaire = {
    "user": {
        "name": "Nom",
        "email": "Email",
        "phone": "Téléphone"
    },
    "medecin": {
      "city": "Ville",
      "cp": "Code Postal",
      "adress": "Adresse"
    },
    "medicaments": {
        "medID": "Numéro médicament",
        "id": "Identifiant",
        "name": "Médicament",
        "maladie": "Maladie"
    },
    "maladies": {
        "id": "Identifiant",
        "name": "Maladie"
    },
    "traitements": {
        "treatment": "Traitement",
        "illness": "Maladie",
        "id": "Identifiant",
        "user": "Utilisateur",
        "quantity": "Quantité",
        "timePerRec": "Nb de fois",
        "recurrence": "Par récurrence",
        "startDate": "Début",
        "endDate": "Fin"
    },
    "contact": {
        "name": "Nom",
        "status": "Status",
        "specialite": "Spécialité",
        "contact": "Contact",
        "adress": "Adresse",
        "days": "Disponibilités",
        "start": "Heure d'ouverture",
        "end": "Heure de fermeture"
    },
    "days": {
        "fr": ["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"]
    },
    "RendezVous": {
        "pro": "Professionnel",
        "name": "Nom",
        "profession": "Profession",
        "day": "Jour",
        "startTime": "Heure de début",
        "endTime": "Heure de fin"
    },
}

